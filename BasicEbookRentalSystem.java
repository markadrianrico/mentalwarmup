import java.util.Scanner;
class Book {
	private String title;
	private String author;
	private int yearPublished;
	private int numberOfCopiesAvailable;
	
	Book() {}
	
	Book(String title, String author, int numberOfCopiesAvailable) {
		this.title = title;
		this.author = author;
		this.numberOfCopiesAvailable = numberOfCopiesAvailable;
	}
	
	String getTitle() {
		return title;
	}
	String getAuthor() {
		return author;
	}	
	int getYearPublished() {
		return yearPublished;
	}	
	int getNumberOfCopiesAvailable() {
		return numberOfCopiesAvailable;
	}
	
	void setTitle(String title) {
		this.title = title;
	}
	void setBookAuthor(String author) {
		this.author = author;
	}	
	void setYearPublished(int yearPublished) {
		this.yearPublished = yearPublished;
	}	
	void setNumberOfCopiesAvailable(int numberOfCopiesAvailable) {
		this.numberOfCopiesAvailable = numberOfCopiesAvailable;
	}
	void addNumberOfCopiesAvailable(int number){
		this.numberOfCopiesAvailable += number;
	}
}



class BasicEbookRentalSystem {
	public static void main(String[] args) {
		
		Book book0 = new Book("System Analysis and Design", "Gary B. Shelly", 2);
		Book book1 = new Book("Android Application", "Corinne Hoisington", 3);
		Book book2 = new Book("Programming Concepts and Logic Formulation", "Rosauro B. Manuel", 4);
		
		System.out.println("ELECTRONIC BOOK RENTAL SYSTEM");
		System.out.println("*".repeat(20));
		System.out.println("0 " + book0.getTitle() + ", " + book0.getAuthor());
		System.out.println("1 " + book1.getTitle() + ", " + book1.getAuthor());
		System.out.println("2 " + book2.getTitle() + ", " + book2.getAuthor());
		System.out.println("*".repeat(20));
		
		Scanner sc = new Scanner(System.in);
		String running = "Y";
		while (running.equals("Y")) {
			running = "undefined";
			System.out.print("CHOOSE A NUMBER TO RENT A BOOK: ");
			try {
				int userBookPreference = sc.nextInt();
				if (userBookPreference == 0){
					if (book0.getNumberOfCopiesAvailable() > 0){
						book0.addNumberOfCopiesAvailable(-1);
						System.out.println("You rented " + book0.getTitle());
					}
					else {
						System.out.println("No copies available");
					}
				}
				else if (userBookPreference == 1){
					if (book0.getNumberOfCopiesAvailable() > 0){
						book0.addNumberOfCopiesAvailable(-1);
						System.out.println("You rented " + book1.getTitle());
					}
					else {
						System.out.println("No copies available");
					}
				}
				else if (userBookPreference == 2){
					if (book2.getNumberOfCopiesAvailable() > 0){
						book2.addNumberOfCopiesAvailable(-1);
						System.out.println("You rented " + book2.getTitle());
					}
					else {
						System.out.println("No copies available");
					}
				}
				else {
					System.out.println("INDEX DOES NOT EXIST, Try Again!");
				}				
			}
			catch (java.util.InputMismatchException e) {
				System.out.println("come on, now. you're better than this :D");
			}
			

			System.out.println("Rent again? Y/N");
			while (!running.equals("Y") && !running.equals("N")) {
				running = sc.nextLine();
			}
		}
	}
}